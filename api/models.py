from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class AnimeGenre(models.Model):
    GENRE_CHOICES = (
     'Action', 'Action'
     'Adventure', 'Adventure'
     'Comedy', 'Comedy'
     'Drama', 'Drama'
     'Slice of Life', 'Slice of Life'
     'Fantasy', 'Fantasy'
     'Magic', 'Magic'
     'Supernatural', 'Supernatural'
     'Horror', 'Horror'
     'Mystery', 'Mystery'
     'Psychological', 'Psychological'
     'Romance', 'Romance'
     'Sci-Fi', 'Sci-Fi'
     'Cyberpunk', 'Cyberpunk'
     'Ecchi', 'Ecchi'
     'Demons', 'Demons'
     'Harem', 'Harem'
     'Josei', 'Josei'
     'Martial Arts', 'Martial Arts'
     'Kids', 'Kids'
     'Historical', 'Historical'
     'Hentai', 'Hentai'
     'Isekai', 'Isekai'
     'Military', 'Military'
     'Mecha', 'Mecha'
     'Music', 'Music'
     'Parody', 'Parody'
     'Police', 'Police'
     'Post-Apocalyptic', 'Post-Apocalyptic'
     'Reverse Harem', 'Reverse Harem'
     'School', 'School'
     'Seinen', 'Seinen'
     'Shoujo', 'Shoujo'
     'Shoujo-ai', 'Shoujo-ai'
     'Shounen', 'Shounen'
     'Shounen-ai', 'Shounen-ai'
     'Space', 'Space'
     'Sports', 'Sports'
     'Super Power', 'Super Power'
     'Tragedy', 'Tragedy'
     'Vampire', 'Vampire'
     'Yuri', 'Yuri'
     'Yaoi', 'Yaoi'
    )
    genre = ArrayField(
        models.CharField(choices=GENRE_CHOICES, default='Action', blank=False)
    )

class AnimeType(models.Model):
    TYPE_CHOICES = (

    )

class Anime(models.Model):
    name = models.TextField(max_length=200, default='Anime Name', blank=False)
    genre = models.ForeignKey(AnimeGenre, related_name='Genres of Animes', on_delete=models.CASCADE)
    release_year = models.DateField(unique_for_year=True, blank=False)
    type = models.CharField(max_length=100, blank=False)
    episodes = models.PositiveSmallIntegerField()
    studio = models.TextField(max_length=200, default='Anime Studio')
    country = models.CharField(max_length=100, default='Japan')
    finished = models.BooleanField(default=False)
    description = models.TextField()
    poster = models.ImageField(upload_to='uploads/%Y/%m/%d/', width_field=500, height_field=800)
    screenshots = models.ImageField(upload_to='uploads/%Y/%m/%d/', width_field=900, height_field=500)
    characters = models.TextField()

    class Meta:
        ordering = ('name', 'release_year')

class Characters(models.Model):
    GENDER_LIST = (
        'Male', 'Male'
        'Woman', 'Woman'
    )
    first_name = models.TextField()
    last_name = models.TextField()
    profession = models.TextField()
    gender = models.CharField(ordering=GENDER_LIST, blank=False, default='M')
    anime = models.ForeignKey(Anime, related_name='From Anime', on_delete=models.CASCADE)
    biography = models.TextField()
    story = models.TextField()
    relatives = models.URLField()
    age = models.PositiveIntegerField()
    birth_date = models.DateField()
    height = models.PositiveIntegerField()
    weight = models.PositiveIntegerField()
    hobby = models.TextField()
    like = models.IntegerField()
    dislike = models.IntegerField()
    avatar = models.ImageField()
    figure = models.ImageField()

    class Meta:
        ordering = ('first_name', 'last_name')